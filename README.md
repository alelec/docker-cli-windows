# Docker CLI
To use the Docker API from inside a Windows container it is useful to have the Docker CLI.

Especially since Windows Server 1709 it's easy to access the Docker API of the host

```
docker run -v //./pipe/docker_engine://./pipe/docker_engine registry.gitlab.com/alelec/docker-cli-windows:latest docker version
```
